console.log("Arthmatic Operators in js")

let a = 45;
let b = 4;
console.log("a + b = ",a + b)
console.log("a - b = ",a - b)
console.log("a * b = ",a * b)
console.log("a / b = ",a / b)
console.log("a ** b = ",a ** b)
console.log("a % b = ",a % b)  // it will print remainder
console.log("a++ = ",a++)  //++a and a++
console.log(a)
console.log("a-- = ",a--)  //--a nad a--


console.log("-------------------------------------------------------------------------------")
console.log("Assignment Operator: ")

let assignment = 1;

assignment += 5
console.log(assignment)

assignment -= 5
console.log(assignment)

assignment *= 5
console.log(assignment)

assignment /= 5
console.log(assignment)

assignment **= 5
console.log(assignment)

assignment &= 5
console.log(assignment)


console.log("-------------------------------------------------------------------------------")
console.log("Comparison Operator")

let comp1 = 10
let comp2 = 100
console.log("comp1 == comp2: ", comp1 == comp2)
console.log("comp1 != comp2 = ", comp1 != comp2)

console.log("comp1 === comp2: ", comp1 === comp2)
console.log("comp1 !== comp2 = ", comp1 !== comp2)

console.log("comp1 > comp2 = ", comp1 > comp2)

console.log("-------------------------------------------------------------------------------")
console.log("Logical Operator: ")

let x = 5
let y = 6

console.log(x<y && x==5)   //both are true then true
console.log(x>y || x==5)   //any one is true then true
console.log(!false)        //It will make it opposite








console.log("-------------------------------------------------------------------------------")
