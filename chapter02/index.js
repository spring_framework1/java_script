console.log("JS chapter 03")

let a = 45;
const b = "Ravi"
let c = null      //all datatypes will be declared as this

{
    let b = 'this'
    console.log(b)
}

console.log(b)

const author = "Ravi"
//let author = "Harry"  //gives error
