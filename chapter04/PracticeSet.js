//1
let a = "Raviraj"
let b = 10

 console.log(a + b)




 //2
 console.log(typeof a)
 console.log(typeof b)
 console.log(typeof (a+b))



 //3
 const c ={
     name: "Ravi",
     section: 1,
     isStudent: false
 }

 c['friend'] = "Harry"  //this will add one more key and obj to the c object
 console.log(c)
 //c = "Ravi"   // Error


 //5
 const dict = {
     Apple: "This a Fruit",
     Ball: "This a tool by which we can play cricket",
     Cat: "This is Animal"
 }

 console.log(dict.Apple)
 console.log(dict.Ball)


