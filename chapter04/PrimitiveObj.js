//nn bb ss u
//[Primitive DataType]
let a = null;
let b = 345;
let c = true
let d = BigInt("567") + BigInt("3")
let e = "Ravi"
let f = Symbol("I am nice Stymbol")
let g
console.log(a, b, c, d, e, f)

console.log(typeof b)
console.log(typeof g)  // undefined


//Object [Non-Primitive Data types]
const item={
    "Harry": true,
    "Ravi":9,
    "David":"ABCD"
}

console.log(item["Ravi"])
console.log(item["David"])